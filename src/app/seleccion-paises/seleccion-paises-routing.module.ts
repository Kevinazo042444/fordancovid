import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SeleccionPaisesPage } from './seleccion-paises.page';

const routes: Routes = [
  {
    path: '',
    component: SeleccionPaisesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SeleccionPaisesPageRoutingModule {}
