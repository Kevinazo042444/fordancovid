import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SeleccionPaisesPageRoutingModule } from './seleccion-paises-routing.module';

import { SeleccionPaisesPage } from './seleccion-paises.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SeleccionPaisesPageRoutingModule
  ],
  declarations: [SeleccionPaisesPage]
})
export class SeleccionPaisesPageModule {}
