import { TestBed } from '@angular/core/testing';

import { ServicioCovidService } from './servicio-covid.service';

describe('ServicioCovidService', () => {
  let service: ServicioCovidService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicioCovidService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
